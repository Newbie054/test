package repository;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import model.Student;

public interface Repo extends JpaRepository<Student, Long> {
    List<Student> findByKelas(long kelas);
}