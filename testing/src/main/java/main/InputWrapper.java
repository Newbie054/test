package main;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Table;

public class InputWrapper {
    
    private String name;
    private long age;
    private long kelas;
    
    public InputWrapper(){
        
    }
    public InputWrapper(String name, long age, long kelas){
        this.name = name;
        this.age = age;
        this.kelas = kelas;
    }
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getAge() {
        return age;
    }

    public void setAge(long age) {
        this.age = age;
    }

    public long getKelas() {
        return kelas;
    }

    public void setKelas(long kelas) {
        this.kelas = kelas;
    }
    
}