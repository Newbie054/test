package main;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "student/")
public class StudentController {
    @Autowired
    private Repo repo;
    
    
    @RequestMapping(value = "/")
    public List allStudent(){
        return(List<Student>) repo.findAll() ;
    }
    
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public OutputWrapper addStudent(@RequestBody InputWrapper student){
        Student newStudent = new Student();
        newStudent.setName(student.getName());
        newStudent.setAge(student.getAge());
        newStudent.setKelas(student.getKelas());
        repo.save(newStudent);
        OutputWrapper out = new OutputWrapper(newStudent.getId(), newStudent.getName(), "Successfully added");
        return out;
    }
    
    @RequestMapping(value="/search/kelas/{kelas}")
    public List Student(@PathVariable long kelas){
        List students = repo.findByKelas(kelas);
        return students;
    }
    
    @RequestMapping(value="/search/name/", method = RequestMethod.GET)
    public List Student(@RequestParam(name="name", required=true, defaultValue="nameHere") String name){
        List students = repo.findByName(name);
        return students;
    }
}