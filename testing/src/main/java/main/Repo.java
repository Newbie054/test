package main;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

public interface Repo extends JpaRepository<Student, Long> {
    List<Student> findByKelas(long kelas);
    List<Student> findByName(String name);
}