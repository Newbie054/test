package main;

public class OutputWrapper {
        private long id;
        private String name;
        private String message;
 
    public OutputWrapper(long id, String name , String message){
        this.id = id;
        this.name = name;
        this.message = message;
    }
        
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

}
