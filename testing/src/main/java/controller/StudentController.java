package controller;

import java.util.List;
import model.Student;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import repository.Repo;

@Controller
public class StudentController {
    @Autowired
    Repo repo;

    @RequestMapping(value = "/")
    public List allStudent(){
        return(List<Student>) repo.findAll() ;
    }
    
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public Student addStudent(@RequestBody Student student){
        Student newStudent = repo.save(new Student(student.getName(), student.getAge(), student.getKelas()));
        return newStudent;
    }
    
    @RequestMapping(value="/search/kelas/{kelas}")
    public List Student(@PathVariable long kelas){
        List students = repo.findByKelas(kelas);
        return students;
    }
}